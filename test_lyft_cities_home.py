from lyft_cities_home_page import LyftCitiesHome
from lyft_api import LyftRegionApi
import lyft_truth_data
import unittest


class TestLyftCitiesHome(unittest.TestCase):

    def setUp(self):
        self.home_page = LyftCitiesHome()

    def test_home_page_title(self):
        self.home_page.visit_cities_home()
        self.assertEqual(self.home_page.get_title(), lyft_truth_data.hp_title)

    def test_jumbotron_background_image(self):
        self.home_page.visit_cities_home()
        background_image = self.home_page.get_jumbotron_background_image()
        self.assertEqual(background_image, lyft_truth_data.hp_jumbotron_background_image)

    def test_page_states_match_api_states(self):
        # Get states from lyft api
        region_api = LyftRegionApi()
        api_states = sorted(region_api.get_api_states())

        # Get states from home page
        self.home_page.visit_cities_home()
        page_states = self.home_page.get_list_of_states()

        self.assertEqual(api_states, page_states)

    def test_page_cities_match_api_cities(self):
        # Get cities from lyft api
        region_api = LyftRegionApi()
        api_cities = sorted(region_api.get_api_cities())

        # Get cities from home page
        self.home_page.visit_cities_home()
        page_cities = sorted(self.home_page.get_list_of_cities())

        self.assertEqual(api_cities, page_cities)

    def test_drop_down_data_city(self):
        self.home_page.visit_cities_home()
        cities_displayed = self.home_page.get_dropdown_options('Tem')

        self.assertEqual(cities_displayed, lyft_truth_data.city_test_results)

    def tearDown(self):
        self.home_page.close()
