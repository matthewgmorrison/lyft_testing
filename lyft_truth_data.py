#####################
# Cities Home Page
#####################
hp_title = u'Cities - Lyft'
hp_jumbotron_background_image = {
    'background-image': u'url("https://www.lyft.com/images/vendor-lyft/photography/cities-were-in.f889f07d.jpg")',
    'background-position': u'50% 100%',
    'repeat': u'',
    'opacity': u'1',
    'background-size': u'cover'
}

#TODO get city_test_results from google api
city_test_results = [
    u'TempeAZ, United States',
    u'TemeculaCA, United States',
    u'Temple CityCA, United States',
    u'TempleTX, United States',
    u'TempletonCA, United States',
]

#####################
# City Page
#####################



