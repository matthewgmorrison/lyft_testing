from lyft_base_page import LyftBasePage
from lyft_config import cities_home_url


def edit_html_city_name(city_name):
    return city_name.replace(' ', '-').replace('.', '').replace(',', '').lower()


class LyftCitiesHome(LyftBasePage):

    def visit_cities_home(self):
        with self.wait_for_page_load():
            self.browser.get(cities_home_url)

    def get_jumbotron(self):
        return self.browser.find_element_by_css_selector('.jumbotron.ng-scope')

    def get_list_of_states(self):
        city_list_anchor = self.browser.find_element_by_class_name('city-list')
        state_elements = city_list_anchor.find_elements_by_css_selector('.province-name.ng-binding')
        return [state.get_attribute('innerHTML') for state in state_elements]

    def get_list_of_cities(self):
        city_list_anchor = self.browser.find_element_by_class_name('city-list')
        cities_lists = city_list_anchor.find_elements_by_tag_name('ul')
        page_cities = []
        for city_list in cities_lists:
            cities = city_list.find_elements_by_class_name('ng-binding')
            page_cities += cities
        return [edit_html_city_name(pc.get_attribute('innerHTML')) for pc in page_cities]

    def get_dropdown_options(self, text_input):
        search_box = self.browser.find_element_by_id('myLocation')
        search_box.send_keys(text_input)
        self.wait_until_text_present('class_name', 'pac-item', text_input)

        matches = self.browser.find_elements_by_class_name('pac-item')
        return [match.text for match in matches]

    def get_jumbotron_inner_div(self):
        jumbotron = self.get_jumbotron()
        return jumbotron.find_element_by_tag_name('div')

    # TODO fix this so I see the opacity issue I see when ran manually
    def get_jumbotron_background_image(self):
        jumbotron_div = self.get_jumbotron_inner_div()
        # self.wait_until_visible(jumbotron_div)
        from time import sleep
        sleep(3)

        jumbotron_background = {}
        jumbotron_background['background-image'] = jumbotron_div.value_of_css_property('background-image')
        jumbotron_background['repeat'] = jumbotron_div.value_of_css_property('repeat')
        jumbotron_background['background-position'] = jumbotron_div.value_of_css_property('background-position')
        jumbotron_background['opacity'] = jumbotron_div.value_of_css_property('opacity')
        jumbotron_background['background-size'] = jumbotron_div.value_of_css_property('background-size')
        return jumbotron_background
