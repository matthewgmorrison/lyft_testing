from requests import get
from requests.models import Response
from lyft_config import lyft_region_api_url


class LyftRegionApi(object):

    def __init__(self):
        self.lyft_region_api_url = lyft_region_api_url
        self.response_dict = Response()

    def get_lyft_region_api(self):
        self.response_dict = get(self.lyft_region_api_url).json()['regions']

    def get_api_cities(self):
        self.get_lyft_region_api()
        return self.response_dict.keys()

    def get_api_states(self):
        states = []
        for city in self.get_api_cities():
            state = self.response_dict[city]['state']['name']
            if state not in states:
                states.append(state)
        return states

    def get_api_standard_city_pricing(self, city):
        self.get_lyft_region_api()
        return self.response_dict[city.lower()]['pricing'][0]['fields']

    def get_api_plus_city_pricing(self, city):
        self.get_lyft_region_api()
        return self.response_dict[city.lower()]['pricing'][1]['fields']
