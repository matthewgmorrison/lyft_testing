import contextlib
import lyft_config
from selenium import webdriver
from selenium.webdriver.support.expected_conditions import staleness_of
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By


class LyftBasePage(object):

    def __init__(self):
        # browser = os.environ.get('BROWSER')
        # if browser.lower() == 'phantomjs':
        #     self.browser = webdriver.PhantomJS()
        # else:
        self.browser = webdriver.Firefox()

    def wait_until_text_present(self, by, locator, text):
        if by == 'class_name':
            WebDriverWait(self.browser, lyft_config.WAIT_TIME).until(
                EC.text_to_be_present_in_element((By.CLASS_NAME, locator), text)
            )
    def wait_until_visible(self, element):
        WebDriverWait(self.browser, lyft_config.WAIT_TIME).until(
            EC.visibility_of(element)
        )

    @contextlib.contextmanager
    def wait_for_page_load(self, timeout=30):
        old_page = self.browser.find_element_by_tag_name('html')
        yield
        WebDriverWait(self, timeout).until(staleness_of(old_page))

    def get_title(self):
        return self.browser.title

    def maximize(self):
        self.browser.maximize_window()

    def close(self):
        self.browser.close()

    def __del__(self):
        try:
            self.browser.quit()
        finally:
            pass
