from lyft_base_page import LyftBasePage
from lyft_config import cities_home_url
from selenium.common.exceptions import NoSuchElementException


def edit_price_header(header):
    return (header[0].lower() + header[1:]).replace(' ', '')


def edit_price(price):
    return float(price[1:])


class LyftCityPage(LyftBasePage):

    def visit_city_page(self, city):
        with self.wait_for_page_load():
            self.browser.get(cities_home_url+city)

    def get_pricing(self, price_type):
        table_anchor = self.browser.find_element_by_class_name('pricing')
        if price_type == 'standard':
            table = table_anchor.find_elements_by_tag_name('table')[0]
        else:
            table = table_anchor.find_elements_by_tag_name('table')[1]
        table_rows = table.find_elements_by_tag_name('tr')
        prices = {}
        for row in table_rows:
            try:
                header = row.find_element_by_tag_name('th').text
                price = row.find_element_by_tag_name('td').text
                prices[edit_price_header(header)] = edit_price(price)
            except NoSuchElementException:
                pass
        return prices

    def get_standard_pricing(self):
        return self.get_pricing('standard')

    def get_plus_pricing(self):
        return self.get_pricing('plus')
