from lyft_city_page import LyftCityPage
from lyft_api import LyftRegionApi
import lyft_config
import unittest


class TestLyftCitiesHome(unittest.TestCase):

    def setUp(self):
        self.city_page = LyftCityPage()

    def test_lyft_standard_pricing(self):
        # Get pricing from API
        region_api = LyftRegionApi()
        api_pricing = region_api.get_api_standard_city_pricing(lyft_config.test_city)

        # remove empty values returned from the api (ex. surcharges)
        app = dict(api_pricing)
        for header in app:
            if not api_pricing[header]:
                del api_pricing[header]

        # Get pricing from city page
        self.city_page.visit_city_page(lyft_config.test_city)
        self.city_page.maximize()
        page_pricing = self.city_page.get_standard_pricing()

        self.assertEqual(page_pricing, api_pricing)

    def test_lyft_plus_pricing(self):
        # Get pricing from API
        region_api = LyftRegionApi()
        api_pricing = region_api.get_api_plus_city_pricing(lyft_config.test_city)

        # remove empty values returned from the api (ex. surcharges)
        app = dict(api_pricing)
        for header in app:
            if not api_pricing[header]:
                del api_pricing[header]

        # Get pricing from city page
        self.city_page.visit_city_page(lyft_config.test_city)
        self.city_page.maximize()
        page_pricing = self.city_page.get_plus_pricing()

        self.assertEqual(page_pricing, api_pricing)

    def tearDown(self):
        self.city_page.close()
