This is a set of tests for the Lyft Cities website. 

The easiest way to run this is on a unix system with pip and virtualenv installed:

```
#!shell

virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
nosetests
```